using Unity.MLAgents;
using Unity.MLAgents.Sensors;
using Unity.MLAgents.Actuators;
using UnityEngine;

public class CubeAgent : Agent
{
    [Header("Environment Objects")]
    public Transform Target;
    public Transform SafeZone;

    [Header("Movement Parameters")]
    [SerializeField] private float speedMultiplier = 0.1f;
    [SerializeField] private float rotationSpeed = 200f;

    [Header("Gameplay")]
    [SerializeField] private float rewardTouchTarget = 0.5f;
    [SerializeField] private float rewardReachSafeZone = 0.5f;
    [SerializeField] private float penaltyFallOff = -2.0f;  // Penalty for falling off
    [SerializeField] private float penaltyRepeatTarget = -0.005f;  // Penalty for touching the target after it has already been touched
    [SerializeField] private float penaltyWasteTime = -0.0001f;  // Penalty for touching the target after it has already been touched

    private bool _isTargetTouched = false;

    public override void OnEpisodeBegin()
    {
        ResetAgent();
        MoveTargetToRandomLocation();
    }

    private void ResetAgent()
    {
        _isTargetTouched = false;
        this.transform.localPosition = new Vector3(0, 0.5f, 0);
        this.transform.localRotation = Quaternion.identity;
    }

    private void MoveTargetToRandomLocation()
    {
        Target.localPosition = new Vector3(Random.Range(-4, 4), 0.5f, Random.Range(-4, 4));
    }

    public override void CollectObservations(VectorSensor sensor)
    {
        sensor.AddObservation(transform.localPosition);
        sensor.AddObservation(_isTargetTouched);
    }

    public override void OnActionReceived(ActionBuffers actionBuffers)
    {
        ProcessMovement(actionBuffers);
        CheckIfFellOff();
        AddReward(penaltyWasteTime);
    }

    private void ProcessMovement(ActionBuffers actionBuffers)
    {
        float rotation = actionBuffers.ContinuousActions[0] * rotationSpeed * Time.deltaTime;
        transform.Rotate(0, rotation, 0);

        float moveForward = actionBuffers.ContinuousActions[1] * speedMultiplier;
        transform.Translate(0, 0, moveForward);
    }

    private void CheckIfFellOff()
    {
        if (transform.localPosition.y < -1)  // Assuming -1 is the threshold
        {
            AddReward(penaltyFallOff);
            EndEpisode();
            //Debug.Log("Agent fell off and received penalty.");
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("SafeZone") && _isTargetTouched)
        {
            AddReward(rewardReachSafeZone);
            EndEpisode();
            //Debug.Log("Agent reached safe zone and episode ended.");
        }
        else if (other.CompareTag("Target"))
        {
            if (!_isTargetTouched)
            {
                _isTargetTouched = true;
                AddReward(rewardTouchTarget);
                //Debug.Log("Agent touched target and received reward.");
            }
            else
            {
                //AddReward(penaltyRepeatTarget);
                //Debug.Log("Agent touched target again and received penalty.");
            }
        }
    }

    public override void Heuristic(in ActionBuffers actionsOut)
    {
        var continuousActionsOut = actionsOut.ContinuousActions;
        continuousActionsOut[0] = Input.GetAxis("Vertical");
        continuousActionsOut[1] = Input.GetAxis("Horizontal");
    }
}
